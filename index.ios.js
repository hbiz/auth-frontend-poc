/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {
  AppRegistry,
  Component,
  StyleSheet,
  Text,
  View,
  Linking,
  WebView
} from 'react-native';

var WEBVIEW_REF = 'webview';
var BGWASH = 'rgba(255,0,255,0.8)';

var appContainer = React.createClass({
 render(){
  return (<InstaAuth/>)
 }
});
var InstaAuth = React.createClass(
  {
    getInitialState() {
      // var postLoading = this.props.postLoading
      // console.log('postLoading', postLoading);

      return {
        loggedIn: false,
      };
    },
    componentDidMount(){
      Linking.addEventListener('url', this._handleOpenURL);
      //Linking.openURL('https://www.instagram.com/oauth/authorize/?client_id=b676a5613b8447c780422c4ee80d1386&redirect_uri=miracle://&response_type=token');
    },

    _handleOpenURL(event) {
      console.log(event.url);
    },

    _handleWebViewRequest(req){
      console.log('request: ',req.url);
      const searchToken = '?userData=';
      const tokenStrIndex = req.url.indexOf(searchToken);
      if(tokenStrIndex == -1){
        //ignore it and move on
        return true;
      }else{
        const userDataStr = req.url.slice(tokenStrIndex+searchToken.length,req.url.length);
        console.log('userDataStr: ',userDataStr);
        console.log('decoded: ',decodeURIComponent(userDataStr));
        var userData = JSON.parse(decodeURIComponent(userDataStr));
        console.log('userData: ',userData);
        this.setState(
          {
            ...this.state,
            loggedIn: true,
            userData: userData
          });
        return false;
      }
    },

    render() {
      return (
        this.state.loggedIn?<Home userData={this.state.userData}/>
          :<Signin requestHandler={this._handleWebViewRequest.bind(this)}/>
      );
    }
  }
);
/*
class instaAuth extends Component {

  state = {
    loggedIn: false,
  };

  componentDidMount(){
    Linking.addEventListener('url', this._handleOpenURL);
    //Linking.openURL('https://www.instagram.com/oauth/authorize/?client_id=b676a5613b8447c780422c4ee80d1386&redirect_uri=miracle://&response_type=token');
  }

  _handleOpenURL(event) {
    console.log(event.url);
  }

  _handleWebViewRequest(req){
    console.log('request: ',req);
    const searchToken = '?userData=';
    const tokenStrIndex = req.url.indexOf(searchToken);
    if(tokenStrIndex == -1){
      //ignore it and move on
      return true;
    }else{
      const userDataStr = req.url.slice(tokenStrIndex+searchToken.length,req.url.length);
      console.log('userDataStr: ',userDataStr);
      console.log('decoded: ',decodeURIComponent(userDataStr));
      var userData = JSON.parse(decodeURIComponent(userDataStr));
      console.log('userData: ',userData);
      this.setState(
        {
          ...this.state,
          loggedIn: true,
          userData: userData
        });
      return false;
    }
  }

  render() {
    return (
      this.state.loggedIn?<Home userData={this.state.userData}/>
                          :<Signin requestHandler={this._handleWebViewRequest.bind(this)}/>
    );
  }
}
*/

class Signin extends Component {
  render() {
    return (
      <View style={styles.container}>
        <WebView  ref={WEBVIEW_REF} style={styles.webview}
                  source={{uri:'http://localhost:1337/gitcallback?mode=select'}}
                  onShouldStartLoadWithRequest={this.props.requestHandler}
                  contentInset={{top:0,left:-8,right:0,bottom:-8}}
        />
      </View>
    );
  }
}

class Home extends Component {
  render(){
    return (
      <View style={[styles.container,{paddingTop:20}]}>
        <Text>Logged in!</Text>
        <Text>Name: {this.props.userData.displayName}</Text>
        <Text>email: {this.props.userData.email}</Text>
        <Text>Token: {this.props.userData.token}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F5F4FF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  webview:{
  }
});

AppRegistry.registerComponent('instaAuth', () => appContainer);
